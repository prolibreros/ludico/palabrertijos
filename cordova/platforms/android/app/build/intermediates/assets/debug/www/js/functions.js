var old   = false,
    timer = null,
    time  = 0,
    lang_prefix,
    lang;

// Carga las palabras; viene de: https://gist.github.com/NikaZhenya/156c6cdc212d8ece11811c12bb9445a3
function request (url, create) {
  var xhr = new XMLHttpRequest();

  // Inicio de la petición
  xhr.open('GET', url);
  xhr.responseType = 'text';
  xhr.send();

  // Cuando está cargando
  xhr.onprogress = function (e) {
    if (e.lengthComputable) {
      console.log(parseInt((e.loaded / e.total) * 100) + '%');
    } else {
      console.log(e);
    }
  };

  // Cuando está listo
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);

      if (create) {
        localStorage.setItem('current_file', url.split('/')[2].split('.')[0]);
        create_data(result);
      } else {
        change_slider(result.words.length);
      }
    }
  };

  // Cuando hay error
  xhr.onerror = function (e) {
    console.log(e);
  };
}

// Detecta si es dispositivo móvil; viene de: https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
function detect_mobile () {
  var check = 'mouseup';
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = 'touchend';})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

// Reordena conjuntos de manera aleatoria; viene de: https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

// Le da formato a la cifra; viene de: http://cwestblog.com/2011/06/23/javascript-add-commas-to-numbers/
function format_num (n) {
  return (n + '').replace(/(\d)(?=(\d{3})+$)/g, '$1&#8201;');
}


// Obtiene el botón
function get_btn (e) {
  var p;

  // Va subiendo de padres hasta encontrar uno que tenga identificador
  function find_id (par) {
    if (par.id != '') {
      p = par;
    } else {
      find_id(par.parentNode);
    }
  }

  if (e.target.id != '') {
    return e.target;
  } else {
    find_id(e.target.parentNode);
    return p;
  }
}

// Inicia el juego
function init () {
  var saved = localStorage.getItem('game'),
      menu  = document.createElement('div'),
      score;

  // Crea cada botón
  function build_btn (id, label) {
    var div = document.createElement('div');

    div.id = id;
    div.innerHTML = '<p>' + label  + '</p>';
    div.classList.add('menu-btn');
    div.addEventListener(detect_mobile(), function (e) {menu_btn_fn(e)}, true);

    return div;
  }

  // Comprueba si ya hay top guardados
  for (var key in localStorage) {
    if (/top_/.test(key)) {
      score = true;
    }
  }

  menu.id = 'menu';
  menu.appendChild(build_btn('new', lang.new));
  if (saved) {
    menu.appendChild(build_btn('continue', lang.continue));
  }
  if (score) {
    menu.appendChild(build_btn('scores', lang.scores));
  }
  menu.appendChild(build_btn('about', lang.about));
  
  document.body.appendChild(menu);
}

// Procede según el botón del menú presionado
function menu_btn_fn (e) {
  var el = get_btn(e);

  // Limpia el documento
  document.body.innerHTML = '';

  // Inicializa la función según el botón
  switch (el.id) {
    case 'new':
      init_new();
      break;
    case 'continue':
      old = true;
      init_game();
      break;
    case 'scores':
      init_scores();
      break;
    case 'about':
      init_about();
      break;
  }
}

// Inicia un nuevo juego
function init_new () {
  var start  = document.createElement('div'),
      select = document.createElement('select');

  // Crea cada opción
  function create_div (id, caption, max, min) {
    var div     = document.createElement('div'),
        label   = document.createElement('p'),
        value   = document.createElement('p'),
        slider  = document.createElement('input');

    div.id          = id;
    label.innerHTML = caption;
    slider.id       = id == 'new-total' ? 'val-tot' : 'val-dis'
    slider.type     = 'range';
    slider.min      = min;
    slider.max      = max;
    slider.value    = parseInt(max / 2);
    value.innerHTML = '<b>' + slider.value + '</b>';

    // Cambia el valor cuando el usuario modifica el deslizador
    slider.oninput  = function () {
      value.innerHTML = '<b>' + this.value + '</b>';
    }

    div.classList.add('menu-slider');
    div.appendChild(label);
    div.appendChild(value);
    div.appendChild(slider);
    document.body.appendChild(div);
  }

  // Crea la caja de selección
  select.addEventListener('change', function () {load(false)}, true);
  for (var i = 0; i < lang.dics.length; i++) {
    var option = document.createElement('option');

    if (i == 0) {
      option.setAttribute('selected', 'selected');
    }

    option.innerHTML = lang.dics[i][0];
    option.value     = lang.dics[i][1];
    select.appendChild(option);
  }
  document.body.appendChild(select);

  // Crea los deslizadores
  create_div('new-total', lang.challenges, 100, 1);
  create_div('new-distribution', lang.balance, 100, 0);

  // Crea el botón de inicio
  start.id = 'start';
  start.innerHTML = '<p>' + lang.start + '</p>';
  start.classList.add('menu-btn');
  start.addEventListener(detect_mobile(),  function () {load(true)}, true);
  document.body.appendChild(start);

  load(false);
  add_end(false);
}

// Carga el diccionario
function load (start) {
  var select = document.getElementsByTagName('select')[0];
  request('./json/' + select.value + '.json', start);
}

// Cambia unidades del deslizador para total de ítems
function change_slider (total) {
  var div    = document.getElementById('new-total'),
      p      = div.getElementsByTagName('p')[1],
      slider = div.getElementsByTagName('input')[0],
      half   = parseInt(total / 2);

  p.innerHTML  = '<b>' + half + '</b>';
  slider.max   = total;
  slider.value = half;
}

// Genera los datos del juego
function create_data (words) {
  var total         = document.getElementById('val-tot').value,
      distribution  = parseInt((document.getElementById('val-dis').value * total) / 100),
      words         = shuffle(words.words),
      all_words     = words.slice(),
      final_words   = words.splice(0, total),
      total_options = 4,
      game_obj      = {answered: 0, questions: []},
      game          = game_obj.questions;

  // Iteración que añadirá cada dato
  for (var i = 0; i < final_words.length; i++) {
    var q = distribution > 0 ? final_words[i].l1 : final_words[i].l2,
        a = distribution > 0 ? final_words[i].l2 : final_words[i].l1,
        o = [a.trim()];

    // Elige respuestas incorrectas
    function pick_option (opt, ans) {

      // Si todavía hay espacio disponible
      if (opt.length < total_options) {
        var word        = all_words[Math.floor(Math.random() * all_words.length)],
            option      = distribution > 0 ? word.l2.trim() : word.l1.trim(),
            reverse_opt = distribution > 0 ? word.l1.trim() : word.l2.trim();

        // Si la respuesta ya está elegida o coincide la pregunta, vuelve a hacer un intento
        for (var j = 0; j < opt.length; j++) {
          if (opt[j] == option || ans == reverse_opt) {
            return pick_option(opt, ans);
          }
        }

        // Guarda la respuesta y va a añadir la siguiente
        opt.push(option);
        return pick_option(opt, ans);
      // Cuando ya está lleno regresa las respuestas de manera aleatoria
      } else {
        return shuffle(opt);
      }
    }

    o = pick_option(o, q.trim());

    // Guarda cada dato como un objeto
    game.push({
      picked: '',
      status: null,
      question: q.trim(),
      answer: a.trim(),
      options: o
    });

    distribution = distribution - 1;
  }

  // Guarda los datos de manera aleatoria y manda a iniciar el juego
  game = shuffle(game);
  localStorage.setItem('game', JSON.stringify(game_obj));
  document.body.innerHTML = '';
  localStorage.removeItem('score');
  init_game();
}

// Inicia un juego guardado
function init_game () {
  var game_obj     = JSON.parse(localStorage.getItem('game')),
      game         = game_obj.questions,
      visible_item = null,
      finish       = true;

  // Añade el puntaje, la calificación y la posición
  document.body.innerHTML += '<p class="score">' + lang.score  + '<b id="score">' + format_score() + '</b></p>';
  document.body.innerHTML += '<p class="track"><b id="grade" answered="' + game_obj.answered + '">' + get_grade(game_obj.answered, game_obj.questions.length)  + '</b></p>';
  document.body.innerHTML += '<p class="track"><b id="position">0  / ' + game.length  + '</b></p>';

  // Iteración para crear cada pregunta
  for (var i = 0; i < game.length; i++) {
    var item = game[i],
        main = document.createElement('div'),
        pos;

    // Caja principal de cada pregunta
    main.id = 'item-' + i;
    main.innerHTML = '<p><b>' + item.question + '</b></p>';
    main.classList.add('item');
    main.setAttribute('picked'  , item.picked);
    main.setAttribute('status'  , item.status);
    main.setAttribute('question', item.question);
    main.setAttribute('answer'  , item.answer);
    main.setAttribute('options' , item.options.join(';'));

    // Obtiene el elemento actualmente visible
    if (item.status == null && visible_item == null) {
      visible_item = item;
    } else if (visible_item == null && i == game.length - 1) {
      visible_item = item;
    }

    // Oculta las cajas excepto la actual
    if (visible_item != item) {
      main.style.display = 'none';
    // Marca la posición actual
    } else {
      change_position(i);
    }

    // Genera las opciones de cada pregunta
    for (var j = 0; j < item.options.length; j++) {
      var div = document.createElement('div');

      // Pone una tache o una palomita si ya se respondió
      function check (right) {
        if (item.options[j] == item.picked) {
          if (right) {
            div.classList.add('correct');
          } else {
            div.classList.add('incorrect');
          }
        }
      }

      div.id = 'opt-' + j;
      div.innerHTML = '<p>' + item.options[j] + '</p>';
      div.classList.add('menu-btn');
      div.classList.add('item-opt');
      div.setAttribute('correct', item.options[j] == item.answer);
      main.appendChild(div);

      // Si no ha sido respondida añade los escuchas a las opciones
      if (item.picked == '') {
        finish = false;
        div.addEventListener(detect_mobile(),  function (e) {verify(e)}, true);
      // Si ya ha sido respondida
      } else {
        // Si la opción es la correcta
        if (div.getAttribute('correct') == 'true') {
          div.classList.add('right');
          check(true);
        // Si la opción es la incorrecta
        } else {
          div.classList.add('wrong');
          check(false);
        }
      }
    }

    // Añade la pregunta
    document.body.appendChild(main);
  }

  add_nav();

  if (finish) {
    add_end(true);
  } else {
    add_end(false);
  }

  // Inicia un temporizador que servirá para el puntaje
  start_timer();
}

// Añade un botón de finalizar cuando se ha respondido todo
function add_end (finish) {
  var div;

  function change_label () {
    if (finish) {
      div.id = 'end';
      div.innerHTML = '<p>' + lang.end + '</p>';
      compare_score();
    } else {
      div.id = 'back';
      div.innerHTML = '<p>' + lang.back + '</p>';
    }
  }

  // Si ya existe el botón
  if (document.getElementById('back') != null) {
    div = document.getElementById('back');
  // Si todavía no existe el botón 
  } else {
    div = document.createElement('div');

    div.id = 'back';
    div.classList.add('menu-btn');
    document.body.appendChild(div);

    div.addEventListener(detect_mobile(),  end, true);
  }

  change_label();
}

// Finaliza el juego
function end () {
  document.body.innerHTML = '';
  setTimeout(function () {
    init();
  }, 100);

  // Reseta el temporizador
  end_timer(true);
}

// Cambia el marcador de posición
function change_position (i) {
  var position = document.getElementById('position');

  position.innerHTML = (i + 1)  + ' / ' + position.innerHTML.split(' / ')[1];
}

// Añade los botones de navegación
function add_nav () {
  var nav = document.createElement('nav');

  // Crea ambos botones de navegación
  function create_nav (left) {
    var div = document.createElement('div');

    div.id = left;
    div.innerHTML = '<p><b>' + (left ? '<' : '>') + '</b></p>';
    div.classList.add('menu-btn');
    div.classList.add('nav-btn');
    nav.appendChild(div);

    div.addEventListener(detect_mobile(),  function (e) {move(e)}, true);
  }

  create_nav(true);
  create_nav(false);
  document.body.appendChild(nav);
}

// Cambia de pregunta
function move (e) {
  var el       = get_btn(e),
      left     = el.id == 'true' ? true : false,
      items    = document.getElementsByClassName('item');

  // Itera para obtener el ítem actual
  for (var i = 0; i < items.length; i++) {
    if (items[i].style.display != 'none') {
      var j;

      // Obtiene la posición del siguiente elemento
      if (left) {
        j = i == 0 ? i : i - 1
      } else {
        j = i == items.length -1 ? i : i + 1;
      }

      // Intercambia vistas
      items[i].style.display = 'none';
      items[j].style.display = '';
      change_position(j);

      // Inicia el temporizador que servirá para el puntaje
      if (items[j].getAttribute('picked') == '' && timer == null) {
        start_timer();
      }

      break;
    }
  }
}

// Inicia el temporizador
function start_timer () {
  timer = setInterval(add_time, 1000);
}

// Añade un segundo al tiempo transcurrido
function add_time () {
  time = time + 1;
}

// Limpia el temporizador
function end_timer (reset) {
  clearInterval(timer);
  timer = null;

  if (reset) {
    time  = 0;
  }
}

// Muestra el top de los puntajes
function init_scores () {
  var div = document.createElement('div');

  // Imprime la puntuación
  function print_score (title, file) {
    var rank = JSON.parse(localStorage.getItem('top_' + file)),
        ol   = '<ol id="top_' + file + '">';

    // Solo si sí existe un top
    if (rank) {
      div.innerHTML += '<h1 class="top-header">' + title + '</h1>';

      for (var i = 0; i < 5; i++) {
        if (typeof rank[i] !== 'undefined') {
          ol += '<li><p>' + format_num(rank[i]) + '</p></li>';
        }
      }

      div.innerHTML += ol + '</ol>';
    }
  }

  // Iteración de cada uno de los diccionarios
  for (var i = 0; i < lang.dics.length; i++) {
    print_score(lang.dics[i][0], lang.dics[i][1]);
  }

  // Añade el elemento
  div.id = 'top';
  document.body.appendChild(div);

  add_end(false);
}

// Carga el puntaje
function load_score () {
  return localStorage.getItem('score') ? parseInt(localStorage.getItem('score')) : 0;
}

// Obtiene el puntaje
function get_score (correct) {
  var points   = 100,
      score    = load_score(),
      label    = document.getElementById('score');

  if (correct) {
    score = score + (points - time);
  } else {
    score = score - time;
  }

  localStorage.setItem('score', score);

  label.innerHTML = format_score();
}

// Da formateo al puntaje
function format_score () {
  var score = load_score();

  if (score < 0) {
    return '<span class="red">' + format_num(score) + '</span>';
  } else {
    return format_num(score);
  }
}

// Revisa si el puntaje puede formar parte del top 5
function compare_score () {
  if (!old) {
    var score   = load_score(),
        rank    = localStorage.getItem('top_' + localStorage.getItem('current_file')) ? JSON.parse(localStorage.getItem('top_' + localStorage.getItem('current_file'))) : [],
        limit   = 5,
        new_top = true;

    // Detecta si en efecto hay un nuevo récord
    if (rank.length >= limit) {
      for (var i = 0; i < rank.length; i++) {
        if (score > rank[i]) {
          new_top = true;
        } else {
          new_top = false;
        }
      }
    }

    // Guarda el top 5 solo si es mayor a cero
    if (score > 0) {
      rank.push(score);
      rank = rank.sort((a, b) => b - a); // De: https://stackoverflow.com/questions/1063007/how-to-sort-an-array-of-integers-correctly
      rank = rank.slice(0, limit);
      localStorage.setItem('top_' + localStorage.getItem('current_file'), JSON.stringify(rank));

      // Señala un nuevo récord
      if (new_top) {
        new_record();
      }
    }
  }
}

// Muestra un texto cuando hay un nuevo récord
function new_record () {
  var label = document.getElementById('score'),
      hold  = 1000;

  // Hace el cambio de etiqueta
  label.id = 'new-record';
  label.innerHTML = lang.new_record;  

  // Restaura el valor antiguo de la etiqueta después de cierto tiempo
  setTimeout(function () {
    label.id = '';
    label.innerHTML = load_score();
  }, hold);
}

// Obtiene la calificación
function get_grade (current, total) {
  return parseInt((parseInt(current) * 100) / total) + ' / 100';
}

// Valida la respuesta a cada pregunta
function verify (e) {
  var el        = get_btn(e),
      par       = el.parentNode,
      els       = par.getElementsByTagName('div'),
      response  = el.getAttribute('correct') == 'true',
      gr        = document.getElementById('grade'),
      grade     = parseInt(gr.innerHTML),
      answered  = parseInt(gr.getAttribute('answered')),
      total     = parseInt(document.getElementById('position').innerHTML.split(' / ')[1]),
      questions = document.getElementsByClassName('item'),
      finish    = true;

  // Evita falsos positivos para detectar nuevos récords
  old = false;

  // Si la respuesta es correcta
  if (response) {
    answered     = answered + 1;
    gr.innerHTML = get_grade(answered, total);

    el.classList.add('correct');
    par.setAttribute('status', true);
    gr.setAttribute('answered', answered);
    get_score(true);
  // Si la respuesta es incorrecta
  } else {
    el.classList.add('incorrect');
    par.setAttribute('status', false);
    get_score(false);
  }

  // Añade el valor de la opción escogida
  par.setAttribute('picked', el.getElementsByTagName('p')[0].innerHTML);

  // Elimina los escuchas en las opciones para deshabilitar su funcionalidad
  for (var i = 0; i < els.length; i++) {
    var new_el = els[i].cloneNode(true);

    new_el.classList.add(els[i].getAttribute('correct') == 'true' ? 'right' : 'wrong');
    par.replaceChild(new_el, els[i]);
  }

  // Verifica si ya todo fue respondido
  for (var i = 0; i < questions.length; i++) {
    if (questions[i].getAttribute('picked') == '') {
      finish = false;
      break;
    }
  }

  if (finish) {
    add_end(true);
  } else {
    add_end(false);
  }

  save();
}

// Guarda el juego
function save () {
  var answered_e  = parseInt(document.getElementById('grade').getAttribute('answered')),
      questions_e = document.getElementsByClassName('item'),
      game_obj    = {answered: answered_e, questions: []};

  // Obtiene todos los elementos de cada pregunta
  for (var i = 0; i < questions_e.length; i++) {
    var item       = questions_e[i],
        picked_e   = item.getAttribute('picked'),
        status_e   = item.getAttribute('status') == 'null' ? null : item.getAttribute('status') == 'true' ? true : false,
        question_e = item.getAttribute('question'),
        answer_e   = item.getAttribute('answer'),
        options_e  = item.getAttribute('options').split(';'),
        question   = {
          picked   : picked_e,
          status   : status_e,
          question : question_e,
          answer   : answer_e,
          options  : options_e
        };

    game_obj.questions.push(question);
  }

  // Guarda de manera local
  localStorage.setItem('game', JSON.stringify(game_obj));

  end_timer(true);
}

// Muestra los créditos
function init_about () {
  var div = document.createElement('div');

  div.id = 'credits';
  for (var i = 0; i < lang.collab.length; i++) {
    var type = lang.collab[i][0],
        name = lang.collab[i][1],
        link = lang.collab[i][2];

    if (type != '') {
      div.innerHTML += '<h1>' + type  + '</h1>';
    }

    if (link != '') {
      div.innerHTML += '<p><b><a target="_blank" href="' + link  + '">' + name  + '</a></b></p>';
    } else {
      div.innerHTML += '<p><b><a>' + name  + '</a></b></p>';
    }
  }

  document.body.appendChild(div);

  add_end(false);
}

// Cuando se manda la aplicación al fondo
document.addEventListener('pause', function () {
  // Pausa el temporizador
  end_timer(false);
}, false);

// Cuando la aplicación está activa
document.addEventListener('resume', function () {
  // Reanuda el temporizador si estaba activo
  if (time != 0) {
    start_timer();
  }
}, false);

// Cuando carga el documento HTML
window.onload = function () {
  lang_prefix = (window.navigator.userLanguage || window.navigator.language).split('-')[0]; 
  lang        = language[lang_prefix]

  if (typeof lang === 'undefined') {
    lang_prefix = 'en';
    lang        = language[lang_prefix];
  }

  init();
};
